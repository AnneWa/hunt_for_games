import './App.css'
import Navigation from './components/Navigation/Navigation';
import Home from './components/Home/Home';

function App() {

  return (
    <div className="App">
      <Home />
      <Navigation />
    </div>
  )
}

export default App
