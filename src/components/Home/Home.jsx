import { useState } from 'react';
import '../Home/Home.css'
import ListOfGames from '../ListOfGames/ListOfGames';


function Home() {

    const [viewList, setViewList] = useState(false);

    function showList() {
        setViewList(true);
    }

    if (!viewList) {
        return (
            <div className='home'>
                <div className="home home__title">
                    <h1 >hunt for games</h1>
                </div>
                <div className='home home__slogan'>
                    <p>Avec Hunt for games, trouvez la perle rare au milieu d'un océan de jeux vidéo !</p>
                </div>
                <div>
                    <button onClick={showList} className='home__button'>Afficher la liste</button>
                </div>
            </div>
        );
    }
    if (viewList) {
        return <ListOfGames />;
    }
}

export default Home;