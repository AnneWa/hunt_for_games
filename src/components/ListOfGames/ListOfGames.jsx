import './ListOfGames.css';
import axios from "axios";
import { useEffect, useState } from 'react';
import SelectedGame from '../SelectedGame/SelectedGame';
import Navigation from '../Navigation/Navigation';



const API_URL = "https://raw.githubusercontent.com/bastienapp/gamesapi/main/db.json";

function ListOfGames() {

  const [games, setGames] = useState([]);
  const [itemSelected, setItemSelected] = useState(null);

  function handleClick(evnt) {
    setItemSelected(evnt);

    console.log('evnt', evnt);
  }


  useEffect(() => {
    axios.get(API_URL)
      .then((response) => {
        console.log(response.data);
        setGames(response.data);
      })
      .catch(function (error) {
        console.log(error)
      })
  }, [])

  return (
    <div>
      {/* ici le composant SearBar */}
      {<Navigation />}
      <ul className="ListOfGames">
        {games.map((game) => (
          <li 
          className="item"
          key={game.id}
          onClick={() => handleClick(game)}
          >
            
            <h1>{game.name}</h1>
            <img className="item__bg-img" src={game.background_image} alt="image du jeu" />
          
          </li>
        ))}
      </ul>


      <div>
        {itemSelected ? <SelectedGame {...itemSelected} /> : null}
      </div>
    </div>
  );
}

export default ListOfGames;
