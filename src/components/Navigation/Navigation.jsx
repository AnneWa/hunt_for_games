import React from "react";
import homeIcon from '../../assets/home.png';
import searchIcon from '../../assets/search.png';
import favoriteIcon from '../../assets/star.png';
import './Navigation.css';

function Navigation() {
    return (
        <div className="NavBar-bottom">
            <div>
                <img className="NavBar-bottom__icon--color" src={homeIcon} alt="icone Accueil" />
                <p>Accueil</p>
            </div>
            <div>
                <img className="NavBar-bottom__icon--color" src={searchIcon} alt="icone recherche" />
                <p>Rechercher</p>
            </div>
            <div>
                <img className="NavBar-bottom__icon--color" src={favoriteIcon} alt="icone favoris" />
                <p>Favoris</p>
            </div>
        </div>
    );
}

export default Navigation