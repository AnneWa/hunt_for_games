import React from "react";
import './SelectedGame.css';

function SelectedGame(props) {

  const { name, background_image, released, genres, short_screenshots } = props;

  return (
    <div className="game">

      <div className="game__details">
        
        <div className="game__details--img">
          <img className="game__bg-img" src={background_image} alt="image du jeu" />
        </div>

        <div className="game__details--txt">

          <div>
            <h2>{name}</h2>
            <p className="game__details--release">{released.split('-').reverse().join('/')}</p>
          </div>

          <div>
            <ul className="game__details--genre">
              {genres.map((genre) => (
                <li key={genre.id}>
                  <p>{genre.name}</p>
                </li>
              ))}
            </ul>
          </div>

        </div>

      </div>

      <ul className="game__screenshot">
        {short_screenshots.map((screenshot) => (
          <li key={screenshot.id}>
            <img className="game__screenshot-img" src={screenshot.image} alt="screenshot du jeu" />
          </li>
        ))}
      </ul>

    </div>
  );
}

export default SelectedGame;
